package com.andrei1058.jschatserver.entity;

import java.util.UUID;

public class Group {

    private UUID uuid;
    private String name;

    /** Create */
    public Group(UUID uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }
}
