package com.andrei1058.jschatserver.entity;

import com.andrei1058.jschatserver.connection.Response;
import com.andrei1058.jschatserver.connection.SessionHandler;
import com.andrei1058.jschatserver.console.Logger;
import com.andrei1058.jschatserver.database.Database;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class User {

    private static List<User> users = new ArrayList<>();

    private String name;
    private SessionHandler sessionHandler;
    //username
    private List<String> cachedFriends = new ArrayList<>();

    /**
     * Create a user.
     *
     * @param name           - username.
     * @param sessionHandler - client connection.
     */
    public User(String name, SessionHandler sessionHandler/*, int friendsVersion*/) {
        this.name = name;
        this.sessionHandler = sessionHandler;
        Logger.info("New user connected: " + getName());
        users.add(this);

        new Thread(() -> {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // Send friend requests
            ArrayList<String> requests = Database.getInstance().getFriendRequests(this);
            if (!requests.isEmpty()) {
                for (String r : requests) {
                    friendRequest(r);
                }
            }

            User u;
            StringBuilder sb = new StringBuilder(Response.FRIENDS_LIST_UPDATE.toString() + "<<-->>");
            String string;
            for (String friend : Database.getInstance().getFriendsListForClient(name)) {
                sb.append(friend).append("<<00>>");
                string = friend.split("<<==>>")[0];
                cachedFriends.add(string);
                u = User.getUser(string);
                if (u != null) u.updateFriendStatus(name, 1);
            }
            sessionHandler.sendMessage(sb.toString());
            //}
        }).start();
    }

    public void setSessionHandler(SessionHandler sessionHandler) {
        this.sessionHandler = sessionHandler;
    }

    /**
     * Get username.
     */
    public String getName() {
        return name;
    }

    /**
     * Send a chat message from user.
     */
    public void messageFromUser(User user, String message) {
        sessionHandler.sendMessage(Response.MESSAGE.toString() + "<<-->>" + user.getName() + "<<-->>" + message);
    }

    /**
     * Check if user is friend.
     */
    public boolean isFriend(String username) {
        return cachedFriends.contains(username);
    }

    /**
     * Receive friend request from user.
     */
    public void friendRequest(User user) {
        friendRequest(user.getName());
    }

    /**
     * Add new friend to list.
     */
    public void updateFriendStatus(String username, int onlineStatus) {
        sessionHandler.sendMessage(Response.UPDATE_FRIEND_STATUS.toString() + "<<-->>" + username + "<<-->>" + onlineStatus);
    }

    /**
     * Send friend request.
     */
    private void friendRequest(String user) {
        sessionHandler.sendMessage("FRIEND_REQUEST<<-->>" + user + "<<-->>" + (User.isUserOnline(user) ? 1 : 0));
    }

    public List<String> getCachedFriends() {
        return cachedFriends;
    }

    /**
     * Remove friend.
     */
    public void removeFriend(String username) {
        cachedFriends.remove(username);
        Database.getInstance().removeFriend(getName(), username);
        User u = User.getUser(username);
        if (u != null) {
            u.getSessionHandler().sendMessage("REMOVED_FROM_FRIEND<<-->>" + name);
        }
    }

    /**
     * Logout user.
     */
    public void logOut() {
        users.remove(this);
        User u;
        for (String friend : Database.getInstance().getFriends(name)) {
            u = User.getUser(friend);
            if (u != null) u.updateFriendStatus(name, 0);
        }
        Logger.debug("User logged out: " + getName());
    }

    /**
     * Get users list.
     */
    public static List<User> getUsers() {
        return users;
    }

    /**
     * Check if user is logged in.
     */
    public static boolean isUserOnline(String username) {
        for (User u : new ArrayList<>(users)) {
            if (u.getName().equalsIgnoreCase(username)) return true;
        }
        return false;
    }

    /**
     * Check username format.
     *
     * @return true if it has a valid format.
     */
    public static boolean validateUsername(String username) {
        if (username.isEmpty()) return false;
        if (username.length() <= 5 || username.length() >= 25) return false;
        if (username.equalsIgnoreCase("username")) return false;
        return Pattern.matches("[A-Za-z0-9_]+", username);
    }

    /**
     * Get user by username.
     */
    public static User getUser(String username) {
        for (User u : new ArrayList<>(users)) {
            if (u.getName().equalsIgnoreCase(username)) return u;
        }
        return null;
    }

    public SessionHandler getSessionHandler() {
        return sessionHandler;
    }
}
