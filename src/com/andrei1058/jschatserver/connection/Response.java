package com.andrei1058.jschatserver.connection;

public enum Response {

    /**
     * When there is a user with this name online.
     */
    ALREADY_CONNECTED,
    /**
     * When the provided username is not registered.
     */
    NOT_REGISTERED,
    /**
     * When the given username has an invalid format.
     */
    INVALID_USERNAME,

    /**
     * When provided password did not match.
     */
    WRONG_PASSWORD,

    /**
     * When the received request has an invalid format.
     */
    INVALID_REQUEST_FORMAT,

    /**
     * When login request has accepted.
     */
    LOGIN_SUCCESS,

    /**
     * When tried to register existing username.
     */
    ALREADY_REGISTERED,

    /**
     * When registered successfully.
     */
    REGISTER_SUCCESS,

    /**
     * New friend to list.
     * <p>
     * username, conversation id
     */
    UPDATE_FRIEND_STATUS,

    /**
     * Friends list on login.
     *
     * username, online_status - next user
     */
    FRIENDS_LIST_UPDATE,

    /** Message from user.
     *
     * username, message base64*/
    MESSAGE,

}
