package com.andrei1058.jschatserver.connection;

import com.andrei1058.jschatserver.console.Logger;
import com.andrei1058.jschatserver.entity.User;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class SessionHandler implements Runnable {

    private static List<SessionHandler> sessionHandlers = new ArrayList<>();

    private Socket socket;
    private Scanner in;
    private PrintWriter out;
    // Track if is still connected
    private boolean connected = false;
    private User user = null;

    /**
     * Create a new server - client session.
     *
     * @param socket - connection.
     */
    public SessionHandler(Socket socket) {
        this.socket = socket;
        sessionHandlers.add(this);
        Logger.debug("New SessionHandler: " + socket.getRemoteSocketAddress().toString());
    }

    @Override
    public void run() {
        try {
            in = new Scanner(socket.getInputStream());
            out = new PrintWriter(socket.getOutputStream(), true);
            connected = true;
        } catch (IOException e) {
            e.printStackTrace();
            in.close();
            out.close();
            System.out.println("Could  not establish IO channels..");
        }

        while (connected) {
            if (in.hasNext()) {
                handleRequest();
            }
        }

        in.close();
        out.close();
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Manage requests from socket.
     */
    private void handleRequest() {
        String[] args = in.nextLine().split("<<-->>");
        RequestHandler.Type request = RequestHandler.Type.valueOf(args[0]);
        //
        for (RequestHandler r : RequestHandler.getRequestHandlerList()) {
            if (r.getType() == request) {
                r.execute(user, this, args.length > 1 ? Arrays.copyOfRange(args, 1, args.length) : new String[]{});
                break;
            }
        }
    }

    /**
     * Send a message to the client.
     */
    public void sendMessage(String message) {
        out.println(message);
    }

    /**
     * Assign user instance.
     * It is required at handleRequest.
     */
    public void assignUser(User user) {
        this.user = user;
    }

    /**
     * Close handler.
     */
    public void close() {
        connected = false;
        in.close();
        out.close();
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get socket instance.
     */
    public Socket getSocket() {
        return socket;
    }

    /**
     * Get list of active sessions.
     */
    public static List<SessionHandler> getSessionHandlers() {
        return sessionHandlers;
    }
}
