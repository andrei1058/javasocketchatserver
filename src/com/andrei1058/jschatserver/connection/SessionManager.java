package com.andrei1058.jschatserver.connection;

import com.andrei1058.jschatserver.Main;
import com.andrei1058.jschatserver.connection.request.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SessionManager {

    private static SessionManager instance;
    private ServerSocket serverSocket;

    /**
     * Create session manager.
     */
    private SessionManager() throws IOException {
        instance = this;
        serverSocket = new ServerSocket(6655);

        // Accept clients
        new Thread(() -> {
            while (Main.alive) {
                try {
                    Socket in = instance.serverSocket.accept();
                    new Thread(new SessionHandler(in)).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * Initialize session manager.
     */
    public static void init() throws IOException {
        if (instance != null) return;
        new SessionManager();

        // Register request handlers
        new Login();
        new Register();
        new Logout();
        new AddFriend();
        new FriendRequestResponse();
        new Message();
        new RemoveFriend();
    }

    /**
     * Get server socket instance.
     */
    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    /**
     * Get session manager instance.
     */
    public static SessionManager getInstance() {
        return instance;
    }
}
