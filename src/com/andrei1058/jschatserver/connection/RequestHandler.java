package com.andrei1058.jschatserver.connection;

import com.andrei1058.jschatserver.entity.User;

import java.util.LinkedList;

public abstract class RequestHandler {

    private static LinkedList<RequestHandler> requestHandlerList = new LinkedList<>();

    private Type type;

    /**
     * RequestHandler type.
     */
    public RequestHandler(Type requestType) {
        this.type = requestType;
        requestHandlerList.add(this);
    }

    /**
     * Get request type.
     */
    public Type getType() {
        return type;
    }

    /**
     * Handle request.
     */
    public abstract void execute(User user, SessionHandler sessionHandler, String[] args);

    /**
     * Get request executors.
     */
    public static LinkedList<RequestHandler> getRequestHandlerList() {
        return requestHandlerList;
    }

    /**
     * RequestHandler types.
     */
    public enum Type {
        REGISTER, LOGIN, LOGOUT, ADD_FRIEND, FRIEND_REQUEST_RESPONSE, MESSAGE, REMOVE_FRIEND
    }
}
