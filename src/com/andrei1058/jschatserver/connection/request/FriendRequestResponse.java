package com.andrei1058.jschatserver.connection.request;

import com.andrei1058.jschatserver.connection.RequestHandler;
import com.andrei1058.jschatserver.connection.SessionHandler;
import com.andrei1058.jschatserver.console.Logger;
import com.andrei1058.jschatserver.database.Database;
import com.andrei1058.jschatserver.entity.User;

public class FriendRequestResponse extends RequestHandler {


    public FriendRequestResponse() {
        super(Type.FRIEND_REQUEST_RESPONSE);
    }

    @Override
    public void execute(User user, SessionHandler sessionHandler, String[] args) {
        // user, response
        if (args.length != 2) return;
        if (!Database.getInstance().isUserRegistered(args[0])) return;

        boolean response = Boolean.parseBoolean(args[1]);

        if (response) {
            Logger.debug("New friendship " + user.getName() + " " + args[0]);
            /*int conversationID =*/
            Database.getInstance().makeFriendship(user.getName(), args[0]);
            User u2 = User.getUser(args[0]);
            if (u2 != null) {
                u2.updateFriendStatus(user.getName(), 1);
                user.updateFriendStatus(args[0], 1);
                u2.getCachedFriends().add(user.getName());
            } else {
                user.updateFriendStatus(args[0], 0);
            }
            user.getCachedFriends().add(args[0]);
        }
        Database.getInstance().removeFriendRequest(user, args[0]);
    }
}
