package com.andrei1058.jschatserver.connection.request;

import com.andrei1058.jschatserver.connection.RequestHandler;
import com.andrei1058.jschatserver.connection.SessionHandler;
import com.andrei1058.jschatserver.database.Database;
import com.andrei1058.jschatserver.entity.User;

public class AddFriend extends RequestHandler {

    public AddFriend() {
        super(Type.ADD_FRIEND);
    }

    @Override
    public void execute(User user, SessionHandler sessionHandler, String[] args) {
        if (args.length != 1) return;
        if (user.getName().equalsIgnoreCase(args[0])) return;
        User u = User.getUser(args[0]);
        // check if is online
        if (u != null){
            u.friendRequest(user);
        }
        Database.getInstance().friendRequest(user, args[0]);
    }
}
