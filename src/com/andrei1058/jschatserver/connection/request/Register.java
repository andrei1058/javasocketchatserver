package com.andrei1058.jschatserver.connection.request;

import com.andrei1058.jschatserver.connection.RequestHandler;
import com.andrei1058.jschatserver.connection.Response;
import com.andrei1058.jschatserver.connection.SessionHandler;
import com.andrei1058.jschatserver.entity.User;
import com.andrei1058.jschatserver.console.Logger;
import com.andrei1058.jschatserver.database.Database;

public class Register extends RequestHandler {

    public Register() {
        super(Type.REGISTER);
    }

    private long last = 0;

    @Override
    public void execute(User user, SessionHandler sessionHandler, String[] args) {
        // prevent spam
        if (last > System.currentTimeMillis()) return;
        last = System.currentTimeMillis() + 3000;

        if (sessionHandler == null) {
            return;
        }
        if (user != null) {
            // Already logged in
            return;
        }
        Logger.debug("New register request from: " + sessionHandler.getSocket().getRemoteSocketAddress().toString());

        if (args.length < 3) {
            // code - username - password - email
            sessionHandler.sendMessage(Response.INVALID_REQUEST_FORMAT.toString());
            return;
        }
        if (Database.getInstance().isUserRegistered(args[0])) {
            sessionHandler.sendMessage(Response.ALREADY_REGISTERED.toString());
            return;
        }
        Database.getInstance().register(args[0], args[1], args[2]);
        Logger.info("New user registered: " + args[0]);
        sessionHandler.sendMessage(Response.REGISTER_SUCCESS.toString());
        sessionHandler.assignUser(new User(args[0], sessionHandler/*, 0*/));
    }
}
