package com.andrei1058.jschatserver.connection.request;

import com.andrei1058.jschatserver.connection.RequestHandler;
import com.andrei1058.jschatserver.connection.Response;
import com.andrei1058.jschatserver.connection.SessionHandler;
import com.andrei1058.jschatserver.console.Logger;
import com.andrei1058.jschatserver.entity.User;
import com.andrei1058.jschatserver.database.Database;

public class Login extends RequestHandler {

    private long last = 0;

    public Login() {
        super(Type.LOGIN);
    }

    @Override
    public void execute(User user, SessionHandler sessionHandler, String[] args) {
        // prevent spam
        if (last > System.currentTimeMillis()) return;
        last = System.currentTimeMillis() + 3000;


        if (sessionHandler == null) {
            return;
        }
        if (user != null) {
            // Already logged in
            return;
        }
        Logger.debug("New login request from: " + sessionHandler.getSocket().getRemoteSocketAddress().toString());

        if (args.length < 2) {
            // code - username - password
            sessionHandler.sendMessage(Response.INVALID_REQUEST_FORMAT.toString());
            return;
        }

        if (!User.validateUsername(args[0])) {
            sessionHandler.sendMessage(Response.INVALID_USERNAME.toString());
            return;
        }
        if (!Database.getInstance().isUserRegistered(args[0])) {
            sessionHandler.sendMessage(Response.NOT_REGISTERED.toString());
            return;
        }
        if (!Database.getInstance().validatePassword(args[0], args[1])) {
            sessionHandler.sendMessage(Response.WRONG_PASSWORD.toString());
            return;
        }
        if (User.isUserOnline(args[0])) {
            //sessionHandler.sendMessage(Response.ALREADY_CONNECTED.toString());
            User.getUser(args[0]).getSessionHandler().sendMessage("LOGOUT");
            User.getUser(args[0]).logOut();
            //return;
        }
        new Thread(()-> {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sessionHandler.sendMessage(Response.LOGIN_SUCCESS.toString());
            sessionHandler.assignUser(new User(args[0], sessionHandler/*, Integer.valueOf(args[2])*/));
        }).start();
    }
}
