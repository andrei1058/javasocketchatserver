package com.andrei1058.jschatserver.connection.request;

import com.andrei1058.jschatserver.connection.RequestHandler;
import com.andrei1058.jschatserver.connection.SessionHandler;
import com.andrei1058.jschatserver.console.Logger;
import com.andrei1058.jschatserver.entity.User;

public class Message extends RequestHandler {

    public Message() {
        super(Type.MESSAGE);
    }

    @Override
    public void execute(User user, SessionHandler sessionHandler, String[] args) {

        if (user == null) {
            // Not logged in
            return;
        }

        User u = User.getUser(args[0]);
        if (u == null) return;
        //Logger.debug("New message from: " + user.getName() + " for: " + u.getName());

        // username, message base64
        if (args.length != 2) return;
        if (!user.isFriend(u.getName())) return;
        u.messageFromUser(user, args[1]);

        //todo update db last message
    }
}
