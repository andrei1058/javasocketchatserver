package com.andrei1058.jschatserver.connection.request;

import com.andrei1058.jschatserver.connection.RequestHandler;
import com.andrei1058.jschatserver.connection.SessionHandler;
import com.andrei1058.jschatserver.database.Database;
import com.andrei1058.jschatserver.entity.User;

import static com.andrei1058.jschatserver.connection.RequestHandler.Type.REMOVE_FRIEND;

public class RemoveFriend extends RequestHandler {

    public RemoveFriend() {
        super(REMOVE_FRIEND);
    }

    @Override
    public void execute(User user, SessionHandler sessionHandler, String[] args) {
        if (args.length != 1) return;
        if (!Database.getInstance().isUserRegistered(args[0])) return;
        if (!user.isFriend(args[0])) return;
        user.removeFriend(args[0]);
    }
}
