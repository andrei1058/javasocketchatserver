package com.andrei1058.jschatserver.connection.request;

import com.andrei1058.jschatserver.connection.RequestHandler;
import com.andrei1058.jschatserver.connection.SessionHandler;
import com.andrei1058.jschatserver.entity.User;

public class Logout extends RequestHandler {

    public Logout() {
        super(Type.LOGOUT);
    }

    @Override
    public void execute(User user, SessionHandler sessionHandler, String[] args) {
        if (user != null) user.logOut();
        sessionHandler.close();
    }
}
