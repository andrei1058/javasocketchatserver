package com.andrei1058.jschatserver;

import com.andrei1058.jschatserver.connection.SessionManager;
import com.andrei1058.jschatserver.console.ConsoleManager;
import com.andrei1058.jschatserver.console.Logger;
import com.andrei1058.jschatserver.database.Database;
import com.andrei1058.jschatserver.entity.User;

import java.io.IOException;
import java.util.ArrayList;

public class Main {

    public static boolean alive = true;

    // Start program
    public static void main(String[] args) {
        Logger.command("Starting server...");

        //Start console manager
        ConsoleManager.init();

        if (!Database.init()) {
            Logger.error("Could not connect to the database! Aborting...");
            Main.stop();
            return;
        }

        //Start accepting users
        try {
            SessionManager.init();
        } catch (IOException e) {
            e.printStackTrace();
            Logger.error("Could not start the server socket! Aborting...");
            Main.stop();
            return;
        }
        Logger.info("Starting server socket on: " + SessionManager.getInstance().getServerSocket().getLocalPort());
    }

    /**
     * Stop server.
     */
    public static void stop() {
        for (User u : new ArrayList<>(User.getUsers())){
            if (u.getSessionHandler() != null) u.getSessionHandler().sendMessage("LOGOUT");
            u.logOut();
        }
        alive = false;
        ConsoleManager.close();
        if (Database.getInstance() != null){
            Database.getInstance().close();
        }
    }
}
