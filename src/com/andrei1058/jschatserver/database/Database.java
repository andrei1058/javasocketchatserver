package com.andrei1058.jschatserver.database;

import com.andrei1058.jschatserver.console.Logger;
import com.andrei1058.jschatserver.entity.User;
import com.sun.xml.internal.messaging.saaj.util.Base64;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Database {

    private static Database instance;

    private Connection connection;
    private String host;
    private int port;
    private String database;
    private String user;
    private String pass;
    private boolean ssl;

    private String users_table = "users", groups_table = "groups", friends_table = "friends_", user_groups = "groups_", group_members = "members_", group_messages = "_messages", conversation = "conversation_", conversations = "conversations", friend_requests = "friend_requests_",
            user_settings = "settings_";

    /**
     * Create and load configuration.
     */
    private Database() {
        instance = this;
        File config = new File("config.properties");
        if (!config.exists()) {
            try {
                boolean created = config.createNewFile();
                if (!created) {
                    Logger.error("Could not create config.properties");
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Properties prop = new Properties();
        try {
            FileInputStream in = new FileInputStream(config);
            prop.load(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (prop.getProperty("database-host") == null) prop.setProperty("database-host", "localhost");
        if (prop.getProperty("database-port") == null) prop.setProperty("database-port", "3306");
        if (prop.getProperty("database-name") == null) prop.setProperty("database-name", "JavSocketChat");
        if (prop.getProperty("database-user") == null) prop.setProperty("database-user", "root");
        if (prop.getProperty("database-password") == null) prop.setProperty("database-password", "bread");
        if (prop.getProperty("database-ssl") == null) prop.setProperty("database-ssl", "false");
        try {
            prop.store(new FileOutputStream(config), "Java Socket Chat Configuration");
        } catch (IOException e) {
            e.printStackTrace();
        }
        host = prop.getProperty("database-host");
        port = Integer.valueOf(prop.getProperty("database-port"));
        database = prop.getProperty("database-name");
        user = prop.getProperty("database-user");
        pass = prop.getProperty("database-password");
        ssl = Boolean.valueOf(prop.getProperty("database-ssl").toUpperCase());
    }

    /**
     * Test connection.
     */
    private boolean testConnection() {
        boolean result = false;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database + "?autoReconnect=false&user=" + user
                    + "&password=" + pass + "&useSSL=" + ssl);
            result = true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return result;
    }

    /**
     * Open connection.
     */
    private void connect() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database + "?autoReconnect=true&user=" + user
                    + "&password=" + pass + "&useSSL=" + ssl);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Close connection.
     */
    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Create database tables.
     */
    private void createTables() {
        // general tables
        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS " + users_table + " (id INT(64) NOT NULL AUTO_INCREMENT PRIMARY KEY, username VARCHAR(255), password VARCHAR(255), email VARCHAR(255), avatar VARCHAR(255));");
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS " + groups_table + " (id INT(64) NOT NULL AUTO_INCREMENT PRIMARY KEY, owner VARCHAR(255), avatar VARCHAR(255), name VARCHAR(255));");
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS " + conversations + " (id INT(255) NOT NULL AUTO_INCREMENT PRIMARY KEY, user1 VARCHAR(255), user2 VARCHAR(255));");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initialize database connection.
     *
     * @return true if connected successfully.
     */
    public static boolean init() {
        new Database();
        boolean initialized = getInstance().testConnection();
        if (initialized) {
            getInstance().connect();
            getInstance().createTables();
            Logger.info("Database connected successfully!");
        }
        return initialized;
    }

    /**
     * Check if user is registered.
     */
    public boolean isUserRegistered(String username) {
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT id FROM " + users_table + " WHERE username = '" + username + "';");
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Validate a user password.
     */
    public boolean validatePassword(String username, String password) {
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT password FROM " + users_table + " WHERE username = '" + username + "';");
            if (rs.next()) {
                String password2 = rs.getString("password");
                if (password2.equalsIgnoreCase(password)) return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Register a user.
     */
    public void register(String username, String password, String email) {
        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO " + users_table + " VALUES (?,?,?,?,?);");
            ps.setInt(1, 0);
            ps.setString(3, password);
            ps.setString(2, username);
            ps.setString(4, email);
            ps.setString(5, "0");
            ps.execute();
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS " + friends_table + username + " (username VARCHAR(255), last_read TIMESTAMP NULL DEFAULT NULL, conversation_id INT(255));");
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS " + friend_requests + username + " (username VARCHAR(255));");
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS " + user_settings + username + " (name VARCHAR(255), value INT(255));");
            connection.createStatement().executeUpdate("INSERT INTO `" + user_settings + username + "` (`name`,`value`) VALUES('friends', '0');");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Check if given users are friends.
     *
     * @return the conversation id.
     */
    public int isFriends(String user1, String user2) {
        int id = -1;
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT conversation_id FROM " + friends_table + user1 + " WHERE username = '" + user2 + "';");
            if (rs.next()) {
                id = rs.getInt("conversation_id");
            }
        } catch (SQLException ignored) {
        }
        return id;
    }

    /**
     * Remove a user from friends list if he exists.
     */
    public void removeFriend(String user1, String user2) {
        try {
            connection.createStatement().executeUpdate("DELETE FROM " + friends_table + user1 + " WHERE username = '" + user2 + "' LIMIT 1;");
            connection.createStatement().executeUpdate("DELETE FROM " + friends_table + user2 + " WHERE username = '" + user1 + "' LIMIT 1;");
            //updateFriendsVersion(user1);
            //updateFriendsVersion(user2);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a group tables.
     *
     * @return groupID or -1 if something went wrong.;
     */
    public synchronized int createGroup(String name, String owner) {
        int id = -1;
        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO " + groups_table + " VALUES (?,?,?,?);");
            ps.setInt(1, 0);
            ps.setString(2, owner);
            ps.setString(3, "0");
            ps.setString(4, Arrays.toString(Base64.encode(name.getBytes())));
            ps.execute();

            ResultSet rs = connection.createStatement().executeQuery("SELECT MAX(id) FROM " + groups_table + ";");
            if (rs.next()) {
                id = rs.getInt("id");
                PreparedStatement s = connection.prepareStatement("CREATE TABLE IF NOT EXISTS " + group_members + id + "(?,?);" +
                        "INSERT INTO " + group_members + id + " VALUES(?,?);" +
                        "CREATE TABLE IF NOT EXISTS " + group_messages + id + " (?,?,?,?);" +
                        "INSERT INTO " + user_groups + id + " VALUES (" + id + ");");
                s.setString(1, "username VARCHAR(255)");
                s.setString(2, "last_read TIMESTAMP");
                s.setString(3, owner);
                s.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
                s.setString(5, "id INT(255) NOT NULL AUTO_INCREMENT PRIMARY KEY");
                s.setString(6, "timestamp TIMESTAMP");
                s.setString(7, "author VARCHAR(255)");
                s.setString(8, "messages VARCHAR(255)");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    /**
     * Create friendship between 2 users.
     *
     * @return -1 if something went wrong else the conversation id.
     */
    public synchronized int makeFriendship(String user1, String user2) {
        int id = -1;
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT `id` FROM `" + conversations + "` WHERE (`user1` = '" + user1 + "' AND `user2` = '" + user2 + "') " +
                    "OR (`user1` = '" + user2 + "' AND `user2` = '" + user1 + "');");
            if (rs.next()) {
                id = rs.getInt("id");
            } else {
                connection.createStatement().executeUpdate("INSERT INTO `" + conversations + "` VALUES ('0', '" + user1 + "', '" + user2 + "');");
                rs = connection.createStatement().executeQuery("SELECT `id` FROM `" + conversations + "` WHERE (`user1` = '" + user1 + "' AND `user2` = '" + user2 + "') " +
                        "OR (`user1` = '" + user2 + "' AND `user2` = '" + user1 + "');");
                if (rs.next()) {
                    id = rs.getInt("id");
                }
            }
            PreparedStatement ps = connection.prepareStatement("INSERT INTO `" + friends_table + user1 + "` VALUES (?,?,?);");
            ps.setString(1, user2);
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps.setInt(3, id);
            ps.execute();

            PreparedStatement ps2 = connection.prepareStatement("INSERT INTO `" + friends_table + user2 + "` VALUES (?,?,?);");
            ps2.setString(1, user1);
            ps2.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps2.setInt(3, id);
            ps2.execute();

            //updateFriendsVersion(user1);
            //updateFriendsVersion(user2);

            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS " + conversation + id + " (id INT(255) NOT NULL AUTO_INCREMENT PRIMARY KEY, author VARCHAR(255), message VARCHAR(255), date INT(255));");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    /**
     * Save fried request.
     */
    public void friendRequest(User user, String target) {
        try {
            connection.createStatement().executeUpdate("INSERT INTO `" + friend_requests + target + "`(`username`) VALUES('" + user.getName() + "');");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get friends request list.
     * List of names.
     */
    public ArrayList<String> getFriendRequests(User user) {
        ArrayList<String> list = new ArrayList<>();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM `" + friend_requests + user.getName() + "`;");
            while (rs.next()) {
                list.add(rs.getString("username"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Remove a user from friend requests.
     */
    public void removeFriendRequest(User mainUser, String requester) {
        try {
            connection.createStatement().executeUpdate("DELETE FROM `" + friend_requests + mainUser.getName() + "` WHERE `username`='" + requester + "';");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get database.
     */
    public static Database getInstance() {
        return instance;
    }

    /**
     * Get friends list version for given user.
     */
    public int getFriendsVersion(String username) {
        int i = 0;

        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT `value` FROM `" + user_settings + username + "` WHERE `name` = 'friends';");
            if (rs.next()) i = rs.getInt("value");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return i;
    }

    /**
     * Increment friends list version for given user.
     */
    /*private void updateFriendsVersion(String username) {
        try {
            connection.createStatement().executeUpdate("UPDATE `" + friends_table + username + "` SET `value`='value+1' WHERE `name` = 'friends';");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }*/

    /**
     * Get a user friends list.
     */
    public LinkedList<String> getFriendsListForClient(String username) {
        LinkedList<String> linkedList = new LinkedList<>();

        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM " + friends_table + username + ";");
            while (rs.next()) {
                linkedList.add(rs.getString("username") + "<<==>>" + rs.getTimestamp("last_read").getTime() + "<<==>>" + (User.isUserOnline(rs.getString("username")) ? 1 : 0));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return linkedList;
    }

    public List<String> getFriends(String username) {
        List<String> list = new ArrayList<>();

        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT username FROM " + friends_table + username + ";");
            while (rs.next()) {
                list.add(rs.getString("username"));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

}
