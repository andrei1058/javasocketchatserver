package com.andrei1058.jschatserver.console;

import java.util.Date;

public class Logger {

    /**
     * Show general info log.
     */
    public static void info(String message) {
        System.out.println("[" + new Date(System.currentTimeMillis()).toString() + " INFO] " + message);
    }

    /**
     * Show a command response.
     */
    public static void command(String message) {
        System.out.println("[" + new Date(System.currentTimeMillis()).toString() + " COMMAND] " + message);
    }

    /**
     * Show error.
     */
    public static void error(String message) {
        System.out.println("[" + new Date(System.currentTimeMillis()).toString() + " ERROR] " + message);
    }

    /**
     * Show debug message.
     */
    public static void debug(String message) {
        System.out.println("[" + new Date(System.currentTimeMillis()).toString() + " DEBUG] " + message);
    }
}
