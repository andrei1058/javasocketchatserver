package com.andrei1058.jschatserver.console.commands;

import com.andrei1058.jschatserver.Main;
import com.andrei1058.jschatserver.console.Command;
import com.andrei1058.jschatserver.console.Logger;

public class Stop extends Command {

    public Stop() {
        super("stop");
    }

    @Override
    public void execute(String[] args) {
        Logger.command("Stopping server...");
        Main.stop();
    }
}
