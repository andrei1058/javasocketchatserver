package com.andrei1058.jschatserver.console;

public abstract class Command {

    private String name;

    /**
     * Create a new command.
     */
    public Command(String name) {
        this.name = name;
    }

    /**
     * Manage your command.
     */
    public abstract void execute(String[] args);

    /**
     * Get command name.
     */
    public String getName() {
        return name;
    }
}
