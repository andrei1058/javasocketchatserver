package com.andrei1058.jschatserver.console;

import com.andrei1058.jschatserver.console.commands.Stop;

import java.util.LinkedList;

public class ConsoleManager {

    private static ConsoleManager instance;

    private Thread thread;
    private LinkedList<Command> commands;

    /**
     * Create the console commands handler.
     */
    private ConsoleManager() {
        instance = this;
        commands = new LinkedList<>();
    }

    /**
     * Initialize console handler.
     */
    public static void init() {
        if (instance != null) return;
        new ConsoleManager();
        instance.thread = new Thread(new CommandsHandler());
        instance.thread.start();
        instance.commands.add(new Stop());
    }

    /**
     * Call this on program close.
     */
    public static void close() {
        if (instance == null) return;
        if (instance.thread == null) return;
        //if (instance.thread.isInterrupted()) return;
        CommandsHandler.disable();
        instance.thread.interrupt();
    }

    /**
     * Get console manager instance.
     */
    public static ConsoleManager getInstance() {
        return instance;
    }

    /**
     * Get console commands list.
     */
    public LinkedList<Command> getCommands() {
        return commands;
    }
}
