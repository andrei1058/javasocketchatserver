package com.andrei1058.jschatserver.console;

import com.andrei1058.jschatserver.Main;

import java.util.Arrays;
import java.util.Scanner;

public class CommandsHandler implements Runnable {

    private static Scanner in;

    @Override
    public void run() {
        in = new Scanner(System.in);

        while (Main.alive) {
            if (in.hasNext()) {
                String[] command = in.nextLine().split(" ");
                for (Command c : ConsoleManager.getInstance().getCommands()) {
                    if (command[0].equalsIgnoreCase(c.getName())) {
                        c.execute(command.length == 0 ? new String[]{} : Arrays.copyOfRange(command, 1, command.length));
                    }
                }
            }
        }

        in.close();
    }

    /**
     * Used to stop the thread.
     */
    public static void disable() {
        in.close();
    }
}
